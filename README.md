# ENVIRONMENT

Il progettino Docebo, che troverete all'interno dello zip è basato sullo stack PHP 7, Apache e MySQL.
Questo ambiente LAMP è posto all'interno di un environment Docker.

# REQUIREMENTS

Il requisito minimo per lanciare il progetto è avere sulla propria macchina installato Docker.
Nel mio caso essendo in ambiente Windows ho utilizzato Docker Desktop, su S.O. Windows 10.

# START-UP

Lo zip una volta scompattato può essere installato, posizionandosi da prompt dei comandi sulla folder unzippata lanciando l'istruzione:

    docker-compose up -d

La quale creerà due containers:

docker ps -a
CONTAINER ID   IMAGE         COMMAND                  CREATED       STATUS       PORTS                                        NAMES
29454d308be4   docebo_php7   "docker-php-entrypoi…"   4 hours ago   Up 4 hours   0.0.0.0:80->80/tcp, 0.0.0.0:8080->8080/tcp   docebo_php7_1
62c6b01b506c   mysql         "docker-entrypoint.s…"   4 hours ago   Up 4 hours   0.0.0.0:3306->3306/tcp, 33060/tcp            docebo_mysql_1

Una volta fatto ciò basterà eseguire semplicemente da POSTMan o da browser l'API in GET

    http://localhost/api.php?node_id={{nodeId}}&language={{language}}[&search_keyword={{searchTerm}}&page_num={{pageNum}}&page_size={{pageSize}}]

# Idea

Il punto focale del progettino è la query findNestedSetModels posta nel file DoceboNodeTreeAdapter.

1. Partendo dalla sottoquery più annidata, l’idea alla base della query contenuta nel metodo findNestedSetModels è quella di selezionare l’iLeft, ossia tutte figure aziendali poste alla nostra sinistra, come nell'esempio di Wikipedia, dove si presuppone che dei potenziali acquirenti nella scelta di un abito osservino il guardaroba da SX a DX.
[SELECT iLeft FROM node_tree WHERE idNode = ".$nodeId."]

2. Pertanto effettuando la join su stessa della tabella, selezioniamo tutti i nodi in cui il Child.Left > Parent.Left e Child.iRight < Parent.iRight, presupponendo un livello(level) maggiore nell’elemento figlio
[                      SELECT Child.*
                      FROM node_tree AS Child, node_tree AS Parent
                      WHERE
                        Child.level = Parent.level + 1
                        AND Child.iLeft > Parent.iLeft
                        AND Child.iRight < Parent.iRight
]

3. Presupponendo che Parent.iLeft sia uguale all'iLeft della prima sottoquery.

4. Join con la tabella dei nomi per conoscere il nome del nodo, filtrare per language ed eventiualmente paginare e filtrare per search_keyword.

SELECT node.idNode, ntn.nodeName
FROM (....)
JOIN node_tree_names as ntn ON ntn.idNode = node.idNode && ntn.language = '".$language."' ";
if(!is_null(searchTerm)){
  $query .= "WHERE LOWER(ntn.nodeName) LIKE LOWER('%".$searchTerm."%') ";
}
$query .= "LIMIT ".$start.",".$pageSize;        

Pertanto ad esempio lanciando l'API http://localhost/api.php?node_id=7&language=english avremo in risposta tutti gli elementi della tabella node_tree dove il livello è pari a 3, dato dalla somma del livello del nodo 7 ossia 2, aggiungendo 1 (punto 2 dell'idea appena esposta), effettueremo poi la join per avere i risultati innestati.

# Critical issue

Sebbene la popolazione del database MySQL avvenga correttamente ed in maniera efficiente grazie all'utilizzo dei volumi nel docker-compose

volumes:
  - "./etc/mysql/tables.sql:/docker-entrypoint-initdb.d/1.sql"
  - "./etc/mysql/data.sql:/docker-entrypoint-initdb.d/2.sql"

Purtroppo non sono riuscito a connettere mysql dall'altro container PHP, file

 docker environment


# PHP 7, Apache, MySQL (LAMP) docker environment
Docker container with php 7 on Apache (httpd) MySQL. Based on an docker-compose.yml file sites/dao/DoceboConnection.php metodo getConnection, non a caso per il mio sviluppo ho utilizzato un altro database su cui avevo replicato le due tabelle.

Per vedere la corretta popolazione del database in dase di start up del progetto è possibile lanciare da prompt le seguenti istruzioni:

 	  docker exec -it #container_id bash

dove #container_id è l'id del container che corrisponde all'immagine Docker "mysql".

Entrare nella cli MySQL:

    mysql -u root -p

Password: root

use docebo;
select * from node_tree;
select * from node_tree_names;


# Constraints

- Per la validazione dei parametri dati in pasto all'endpoint è possibile visionare il file sites/services/DoceboService.php al metodo checkInputParams, che costruirà una stringa di errore nel caso in cui la validazione non viene supera e restituirà tale messaggio nel campo error del JSON di risposta di suddetta API.
- Utilizzo di PHP 7.0.16: immagine contenuta nel Dockerfile posto nella cartella /etc/docker.
- Utilizzo di Apache: immagine contenuta nel Dockerfile posto nella cartella /etc/docker e files di configurazione di apache posti nella cartella /etc/apache2.

# Posizione dei files di deliverables:
-  /docebo/sites/api.php
-  /docebo/config.php             -> in seguito copia to in /opt/config.php nella configurazione dei volumi del docker-compose.yml
-  /docebo/etc/mysql/data.sql
-  /docebo/etc/mysql/tables.sql
