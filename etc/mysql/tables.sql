CREATE DATABASE IF NOT EXISTS docebo;

USE docebo;

CREATE TABLE IF NOT EXISTS node_tree (
  idNode int(11) NOT NULL AUTO_INCREMENT,
  level int(11) NOT NULL,
  iLeft int(11) NOT NULL,
  iRight int(11) NOT NULL,
  PRIMARY KEY (idNode)
);

CREATE TABLE IF NOT EXISTS node_tree_names (
  id int(11) NOT NULL AUTO_INCREMENT,
  idNode int(11) NOT NULL,
  language varchar(50) NOT NULL DEFAULT '',
  nodeName varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (id),
  KEY idNode (idNode)
);
