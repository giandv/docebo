<?php

include($_SERVER['DOCUMENT_ROOT'].'/adapters/DoceboNodeTreeManager.php');

/**
 * Service layer among Controller and business logic.
 *
 * Class NodeTreeService
 * @author Gianluca Di Vincenzo <divincenzogianluca@gmail.com>
 * @package services
 */
class DoceboService {

    private $doceboAdapter = null;

    public function __construct($environment) {
        $doceboManager          = new DoceboNodeTreeManager();
        $this->doceboAdapter    =  $doceboManager->getAdapter($environment);
    }

    /**
     * Validator function
     *
     * @param $params,          request to validate.
     * @return message error
     */
    public function checkInputParams($params) {
        $error = "";

        /* S - node_id (integer, required). */
        if (!isset($params['node_id'])) {
            $error .= 'Missing mandatory params -> node_id. ';
        } else if (!is_numeric($params['node_id'])) {
            $error .= 'Error: Param [node_id] must be a number! ';
        } else if (!$this->existNode($params['node_id'])) {
            $error .= 'Invalid Node ID. ';
        }
        /* E - node_id (integer, required). */

        /* S - language (enum, required). */
        if (!isset($params['language'])) {
            $error .= 'Missing mandatory params -> language. ';
        } else if ($params['language'] != 'italian' && $params['language'] != 'english') {
            $error .= 'Error: Param [language] must be match with "italian" or "english" string! ';
        }
        /* E - language (enum, required). */

        /* S - page_num (integer, optional) : the 0-based identifier of the page to retrieve. */
        if (isset($params['page_num']) && (!is_numeric($params['page_num']) || $params['page_num'] < 0)) {
            $error .= 'Invalid page number request. ';
        }
        /* E - page_num (integer, optional) : the 0-based identifier of the page to retrieve. */

        /* S - page_size (integer, optional) : the size of the page to retrieve, ranging from 0 to 1000. */
        if (isset($params['page_size']) && (!is_numeric($params['page_size'])) || $params['page_size'] < 0 || $params['page_size'] > 1000 ) {
          $error .= "Invalid page size request.";
        }
        /* S - page_size (integer, optional) : the size of the page to retrieve, ranging from 0 to 1000. */

        return $error;
    }

    /**
     * Method to retrieve child nodes by parent nodeId and language.
     *
     * @param $nodeId,      node how identifier parent.
     * @param $language,    language who retrieve.
     * @param $page,        page number.
     * @param $pageSize,    size of pagination.
     * @return mixed
     */
    public function getChildrenNodes($nodeId, $language, $search = null, $page = 0, $pageSize = 100) {
        $start = $page * $pageSize;
        return $this->doceboAdapter->findNestedSetModels($nodeId, $language, $search, $start, $pageSize);
    }

    /**
     * Method to check if exist specific node.
     * @param $nodeId
     * @return mixed
     */
    public function existNode($nodeId) {
        $node = $this->doceboAdapter->findByNodeId($nodeId);
        if (!empty($node)) {
            return true;
        }
        return false;
    }
}
