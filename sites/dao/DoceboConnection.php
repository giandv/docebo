<?php

include('/opt/config.php');

/**
 * DoceboConnection with crediantals present on folder opt/config.php
 *
 * Class DoceboConnection
 * @author Gianluca Di Vincenzo <divincenzogianluca@gmail.com>
 * @package dao
 */
class DoceboConnection {

    private $config;
    protected $conn;

    public function __construct() {
        $this->getConnection();
    }

    public function isConnected() {
        return isset($this->conn) && $this->conn->ping();
    }

    public function getConnection() {
        if (!$this->isConnected()) {
            $this->conn = new mysqli('35.242.185.210',
                'userdb',
                'Rr;79tZZ%!@twbM6',
                'doclit',
                '3306');

            /*$this->conn = new mysqli('db',
                'root',
                'root',
                'docebo');*/

            // die if SQL statement failed
            if ($this->conn->connect_error) {
                die("Connection failed: " . $this->conn->connect_error);
            }
            $this->conn->set_charset('utf8');
        }
        return $this->conn;
    }

    public function close() {
        if ($this->isConnected()) {
            $this->conn->close();
        }
    }
}
