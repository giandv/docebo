<?php

include($_SERVER['DOCUMENT_ROOT'].'/dao/DoceboConnection.php');

/**
 * In this class we expose queries to interact with database.
 *
 * Class DoceboNodeTreeAdapter.
 * @author Gianluca Di Vincenzo <divincenzogianluca@gmail.com>
 * @package adapters
 */
class DoceboNodeTreeAdapter {

    private $connection;

    public function __construct() {
        $doceboSingleton = new DoceboConnection();
        $this->connection = $doceboSingleton->getConnection();
    }

    /**
     * Number of child nodes of the current $nodeId
     *
     * @param $nodeId
     * @return int
     */
    public function countChildren($nodeId) {
        $count = 0;
        $query = "SELECT COUNT(*)
                      FROM node_tree AS Child, node_tree AS Parent
                      WHERE
                        Child.level = Parent.level + 1
                        AND Child.iLeft > Parent.iLeft
                        AND Child.iRight < Parent.iRight
                        AND Parent.iLeft = (SELECT iLeft FROM node_tree WHERE idNode = ".$nodeId.")";

        if ($result = $this->connection->query($query)) {
            $count = $result->fetch_row();
        }

        return $count;
    }

    /**
     * Retrieve by search term parameter.
     *
     * @param $nodeId,      node how identifier parent.
     * @param $language,    langage to filter results
     * @param $searchTerm,  search term to filter result.
     * @param $start,       start pagination
     * @param $pageSize,    size of pagination.
     * @return mixed
     */
    public function findNestedSetModels($nodeId, $language, $searchTerm = null, $start, $pageSize) {
        $query = "SELECT node.idNode, ntn.nodeName
                    FROM (
                      SELECT Child.*
                      FROM node_tree AS Child, node_tree AS Parent
                      WHERE
                        Child.level = Parent.level + 1
                        AND Child.iLeft > Parent.iLeft
                        AND Child.iRight < Parent.iRight
                        AND Parent.iLeft = (SELECT iLeft FROM node_tree WHERE idNode = ".$nodeId.")
                    ) as node
                      JOIN node_tree_names as ntn ON ntn.idNode = node.idNode && ntn.language = '".$language."' ";
          if(!is_null(searchTerm)){
            $query .= "WHERE LOWER(ntn.nodeName) LIKE LOWER('%".$searchTerm."%') ";
          }
          $query .= "LIMIT ".$start.",".$pageSize;
        return $this->fetchResult($query);
    }

    /**
    * Execute query and return results.
    * @param $query,    query string to execute.
    * @return mixed
    */
    private function fetchResult($query) {
        $nodes = [];

        if ($result = $this->connection->query($query)) {
            while ($row = $result->fetch_assoc()) {
                array_push($nodes, [
                    'node_id' => $row['idNode'],
                    'name' => $row['nodeName'],
                    'children' => $this->countChildren($row['idNode'])
                ]);
            }
        }

        $result->close();

        return $nodes;
    }

    /**
     * Method to retrieve a node by $nodeId.
     *
     * @param $nodeId,    node id who filters results.
     * @return mixed
     */
    public function findByNodeId($nodeId) {
        $node = null;
        $query = "SELECT iLeft FROM node_tree WHERE idNode = ".$nodeId;

        if ($result = $this->connection->query($query)) {
            $node = $result->fetch_assoc();
        }

        return $node;
    }
}
