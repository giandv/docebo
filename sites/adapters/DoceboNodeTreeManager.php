<?php

include($_SERVER['DOCUMENT_ROOT'].'/adapters/DoceboNodeTreeAdapter.php');

/**
 * Manager that according to the contract environment returns adapter.
 *
 * Class DoceboNodeTreeManager.
 * @author Gianluca Di Vincenzo <divincenzogianluca@gmail.com>
 * @package adapters
 */
class DoceboNodeTreeManager
{

    /**
     * DoceboNodeTreeManager constructor.
     */
    public function __construct(){}

    /**
     * DoceboNodeTreeManager instatiator.
     * @param string $environment,      at this moment we have only one environment, but in this future this concept can be evolve.
     * @return,                         Adapter Contract.
     */
    public function getAdapter($environment)
    {
        if($environment == 'docebo'){
          return new DoceboNodeTreeAdapter();
        }
        throw new Exception("Unsuppoorted environment.");
    }
}
