<?php

require_once './services/DoceboService.php';

$errors = [];
$nodes  = [];
$method = $_SERVER['REQUEST_METHOD'];

if($method != 'GET'){
    $error = 'Unsupport HTTP method';
}else{
    $doceboService  = new DoceboService('docebo');
    $error       = $doceboService->checkInputParams($_GET);
    if (empty($error)) {
        $nodes = $doceboService->getChildrenNodes(
            $_GET['node_id'],
            $_GET['language'],
            isset($_GET['search_keyword']) ? $_GET['search_keyword'] : '',
            isset($_GET['page_num']) ? $_GET['page_num'] : 0,
            isset($_GET['page_size']) ? $_GET['page_size'] : 100
        );
    }
}
echo json_encode(['nodes' => $nodes, 'error' => $error]);
